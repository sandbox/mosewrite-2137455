AUTHOR/MAINTAINER
======================
Author/Maintainer: Marshall Moseley, mosewrite AT earthlink DOT net

README.txt
======================
Simple YouTube Embedding (SYE) provides Wordpress-like shortcodes for embedding a YouTube 
video in any body text in Drupal 7. It doesn't have a lot of adjustable parameters and 
it's not meant to. That's the 'simple' part.

Upload and activate this module in the usual way. If you are new to Drupal and don't know 
how to do that, you'll find instructions here:

	https://drupal.org/documentation/install/modules-themes/modules-7

There is no admin screen for SYE. This readme is the documentation.

SYE works only on the body text, and only on nodes using the 'Full HTML' filter.

Here's how SYE works.

1. Start with a  YouTube URL. You can't use the YouTube 'share' link that appears below a 
given Youtube video. You have to use the *fully qualified* YouTube URL. 
e.g., 'http://www.youtube.com/watch?v=FRTlT3DEydU', not 'www.youtube.com/watch?v=FRTlT3DEydU', 
or 'youtube.com/watch?v=FRTlT3DEydU'. You must use the 'http,' and you must use the 
'www.'. Fortunately all you have to do to get this is go to the video on Youtube, and 
once it's on screen, copy the URL in your browser's address bar into your OS' clipboard. 
(Command-C on a Mac, Ctrl-C on Windows system).

2. SYE uses unique opening and closing brackets. To open the short code use:

		[yt[

	And to close the short code use:

		]yt]

3. In between put the fully qualified URL. So the end result will look like this:

	[yt[http://www.youtube.com/watch?v=FRTlT3DEydU]yt]

This shortcode will be replaced by YouTube embed code when the page renders. The embed 
code will be an iframe with two css classes: 'simple-yt-embed' for every video, and 
'yt-id-<youtube_id>', which will be a unique class for the individual video.

4. The default height and width is 315x420. To change it add parameters to the shortcode 
using commas. So to change the heigh and width to 400x600 in the above example, you would
use this shortcode:

	[yt[http://www.youtube.com/watch?v=FRTlT3DEydU]yt,400,600]

Enjoy

--Marshall